# CICD Setup  
![Logo Proyek](.gitlab-deploy/img/cicd-deploy-bashscripts.jpg)
## Install Runner Pada Server Engine

**- Download binary**
```
sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
```

**- Berikan izin untuk eksekusi (Permission execute)**
```
sudo chmod +x /usr/local/bin/gitlab-runner
```

**- Buat Gitlab Runner user**
```
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
```

**- Install dan Jalankan service**
```
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
```

```
sudo gitlab-runner start
```
<br></br>
## Register Runner pada server engine, agar terkoneksi dengan gitlab 

```
sudo gitlab-runner register --url https://gitlab.com --registration-token $REGISTRATION_TOKEN
```

1) GitLab URL  
![Logo Proyek](.gitlab-deploy/img/reg1.JPG)
2) Runner Name  
![Logo Proyek](.gitlab-deploy/img/reg2.JPG)
3) Runner Executor - docker  
![Logo Proyek](.gitlab-deploy/img/reg3.JPG)
4) Runner Docker Image - alpine:latest  
![Logo Proyek](.gitlab-deploy/img/reg4.JPG)
![Logo Proyek](.gitlab-deploy/img/reg5.JPG)
5) Check Runner Status  
![Logo Proyek](.gitlab-deploy/img/reg6.JPG)
![Logo Proyek](.gitlab-deploy/img/reg7.JPG)

<br></br>
## Setup CICD Variable

1) Buka Menu **Setting** lalu **CICD** pada Gitlab
![Logo Proyek](.gitlab-deploy/img/set1.JPG)
2) Masukan Variable sesuai dengan kebutuhan seperti tercantum pada file #cicd-setup/gitlab-ci-contoh.yml
![Logo Proyek](.gitlab-deploy/img/set2.JPG)